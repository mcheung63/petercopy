#pragma once

#include <iostream>
#include <windows.h>
using namespace std;

class Harddisk
{
public:
	Harddisk();
	~Harddisk();

	static wchar_t * convertCharArrayToLPCWSTR(const char* charArray);
	static BOOL GetDriveGeometry(const char *dsk, DISK_GEOMETRY *pdg);
	static short ReadSect(const char *dsk, char *&_buff, unsigned int _nsect, unsigned int offset);
	static short WriteSect(const char *dsk, char *&_buff, unsigned int _nsect, unsigned int offset);
	static unsigned long long GetDiskLengthIoctl(const char *dsk);
	static void listVolumnes();
};

