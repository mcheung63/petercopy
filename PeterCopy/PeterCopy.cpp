// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <windows.h>
#include "Harddisk.h"
#include <boost/program_options.hpp>
#include <fstream> 
using namespace boost::program_options;
using namespace std;

const size_t ERROR_IN_COMMAND_LINE = 1;

void dumpArray(char *s, int len) {
	for (int x = 0; x < len; x++) {
		printf("%02x ", (unsigned char)s[x]);
	}
	cout << endl;
}

int main(int argc, char* argv[]) {
	const string version = "1.0";
	namespace po = boost::program_options;
	po::command_line_style::style_t style = po::command_line_style::style_t(
		po::command_line_style::unix_style |
		po::command_line_style::case_insensitive |
		po::command_line_style::allow_long_disguise);
	po::options_description desc("Allowed options");
	desc.add_options()
		("in", po::value<string>())
		("out", po::value<string>())
		("offset", po::value<int>())
		("bytes", po::value<int>(), "no of bytes to read/write")
		("help", "help message")
		("v", "version")
		("l", "list all drives")
		;

	po::variables_map vm;
	try
	{
		po::store(po::parse_command_line(argc, argv, desc, style), vm);
		po::notify(vm);

		if (vm.count("help")) {
			cout << desc << "\n";
			cout << "Example 1, copy bootsector from the first harddisk to file a.bin:" << endl;
			cout << "   petercopy --in \\\\.\\PhysicalDrive0 -out a.bin -bytes 512" << endl;
			cout << "Example 2, copy bootsector from file a.bin to first harddisk, becareful you harddisk will corrupt:" << endl;
			cout << "   petercopy --in a.bin -out \\\\.\\PhysicalDrive0 -bytes 512" << endl;
			cout << "Example 3, list all physical drives" << endl;
			cout << "   petercopy -l" << endl;
			return 0;
		}

		if (vm.count("v")) {
			cout << "version : " << version << "\n";
			return 0;
		}

		if (vm.count("l")) {
			Harddisk::listVolumnes();
			return 0;
		}

		string in;
		string out;
		int offset;
		int bytes;

		if (vm.count("in")) {
			cout << "input was set to " << vm["in"].as<string>() << ".\n";
			in = vm["in"].as<string>();
		}
		else {
			cout << "please specific in parameter";
			return 1;
		}

		if (vm.count("out")) {
			cout << "output was set to " << vm["out"].as<string>() << ".\n";
			out = vm["out"].as<string>();
		}
		else {
			cout << "please specific out parameter";
			return 1;
		}

		if (vm.count("offset")) {
			cout << "output was set to " << vm["offset"].as<int>() << ".\n";
			offset = vm["offset"].as<int>();
		}
		else {
			offset = 0;
		}

		if (vm.count("bytes")) {
			cout << "output was set to " << vm["bytes"].as<int>() << ".\n";
			bytes = vm["bytes"].as<int>();
		}
		else {
			cout << "please specific bytes parameter";
			return 1;
		}

		char *buff = new char[bytes];
		if (in[0] == '\\') {
			const char *dsk = in.c_str();
			Harddisk::ReadSect(dsk, buff, bytes, offset);
			/*for (int x = 0; x < bytes; x++) {
				printf("%02x ", (unsigned char)buff[x]);
			}*/
		}
		else {
			ifstream fin(in, std::ifstream::in | std::ifstream::binary);
			if (!fin.is_open()) {
				cout << "unable to open " << in << endl;
				return 1;
			}
			cout << "read : " << bytes << " bytes" << endl;
			fin.read(buff, bytes);
			fin.close();
		}
		dumpArray(buff, bytes);

		if (out[0] == '\\') {
			const char *dsk = out.c_str();
			Harddisk::WriteSect(dsk, buff, bytes, offset);
		}
		else {
			ofstream fos(out, std::ifstream::out | std::ifstream::binary);
			cout << "write : " << bytes << " bytes" << endl;
			fos.write(buff, bytes);
			fos.close();
		}
	}
	catch (po::error& e)
	{
		std::cerr << "ERROR: " << e.what() << std::endl << std::endl;
		std::cerr << desc << std::endl;
		return ERROR_IN_COMMAND_LINE;
	}
}