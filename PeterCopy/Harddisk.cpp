#include "pch.h"
#include "Harddisk.h"
#include <bitset>
#include <vector>
#include <string>
#include <sstream>

Harddisk::Harddisk()
{
}


Harddisk::~Harddisk()
{
}

wchar_t * Harddisk::convertCharArrayToLPCWSTR(const char* charArray)
{
	wchar_t* wString = new wchar_t[4096];
	MultiByteToWideChar(CP_ACP, 0, charArray, -1, wString, 4096);
	return wString;
}

BOOL Harddisk::GetDriveGeometry(const char *dsk, DISK_GEOMETRY *pdg)
{
	HANDLE hDevice;               // handle to the drive to be examined
	BOOL bResult;                  // results flag
	DWORD junk;                   // discard results

	hDevice = CreateFile(convertCharArrayToLPCWSTR(dsk), // drive
		0,                // no access to the drive
		FILE_SHARE_READ | // share mode
		FILE_SHARE_WRITE,
		NULL,             // default security attributes
		OPEN_EXISTING,   // disposition
		0,                // file attributes
		NULL);            // do not copy file attributes

	if (hDevice == INVALID_HANDLE_VALUE) // cannot open the drive
	{
		return (FALSE);
	}

	bResult = DeviceIoControl(hDevice, // device to be queried
		IOCTL_DISK_GET_DRIVE_GEOMETRY, // operation to perform
		NULL, 0, // no input buffer
		pdg, sizeof(*pdg),     // output buffer
		&junk,                 // # bytes returned
		(LPOVERLAPPED)NULL); // synchronous I/O

	CloseHandle(hDevice);

	return (bResult);
}

short Harddisk::ReadSect(const char *dsk, char *&_buff, unsigned int noOfBytes, unsigned int offset) {
	DWORD dwRead;
	//HANDLE hDisk = CreateFile(convertCharArrayToLPCWSTR(dsk), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	HANDLE hDisk = CreateFile(convertCharArrayToLPCWSTR(dsk), // drive
		GENERIC_READ | GENERIC_WRITE,                // no access to the drive
		FILE_SHARE_READ | // share mode
		FILE_SHARE_WRITE,
		NULL,             // default security attributes
		OPEN_EXISTING,   // disposition
		0,                // file attributes
		NULL);            // do not copy file attributes

	if (hDisk == INVALID_HANDLE_VALUE) // this may happen if another program is already reading from disk
	{
		int lastError = GetLastError();
		cout << "unable to read harddisk ";
		if (lastError) {
			cout << "ERROR_ACCESS_DENIED" << endl;
		}
		else {
			cout << lastError << endl;
		}
		CloseHandle(hDisk);
		return 1;
	}
	SetFilePointer(hDisk, offset, 0, FILE_BEGIN); // which sector to read
	memset(_buff, 0, noOfBytes);
	ReadFile(hDisk, _buff, noOfBytes, &dwRead, 0);  // read sector
	cout << " Read : " << dwRead << " of bytes" << endl;
	CloseHandle(hDisk);
	return 0;
}

unsigned long long Harddisk::GetDiskLengthIoctl(const char *dsk)
{
	HANDLE hDisk = CreateFile(convertCharArrayToLPCWSTR(dsk), // drive
		GENERIC_READ | GENERIC_WRITE,                // no access to the drive
		FILE_SHARE_READ | // share mode
		FILE_SHARE_WRITE,
		NULL,             // default security attributes
		OPEN_EXISTING,   // disposition
		0,                // file attributes
		NULL);            // do not copy file attributes

	if (hDisk == INVALID_HANDLE_VALUE)
	{
		cerr << "CreateFile INVALID_HANDLE_VALUE = " << GetLastError() << endl;
		return 0;
	}

	GET_LENGTH_INFORMATION gli;
	DWORD ret;
	int r = DeviceIoControl(hDisk,
		IOCTL_DISK_GET_LENGTH_INFO,
		NULL,
		0,
		&gli,
		sizeof(gli),
		&ret,
		(LPOVERLAPPED)NULL);
	if (r > 0) {
		LONGLONG nUseSize = gli.Length.QuadPart;
		INT64 sizeGB = nUseSize / 1014 / 1024 / 1024;
		//printf("a1=%I64x %I64u\n", sizeGB, sizeGB);
		return gli.Length.QuadPart;
	}
	else {
		cerr << "DeviceIoControl GET_LENGTH_INFORMATION = " << GetLastError() << endl;

		cout << "Failed to get disk information." << endl;
		DWORD error;
		error = GetLastError();
		HRESULT hRe = HRESULT_FROM_WIN32(error);
		char errorData[10];
		sprintf_s(errorData, "%x", hRe);
		cout << "Error code:" <</*hRe*/errorData << endl;
		CloseHandle(hDisk);
		return -1;
	}
}

void Harddisk::listVolumnes() {
	for (char c = '0'; c <= '9'; c++) {
		string drive = string("\\\\.\\PhysicalDrive") + c;
		const char *dsk = drive.c_str();
		int sector = 0;

		DISK_GEOMETRY pdg;            // disk drive geometry structure
		BOOL bResult;                 // generic results flag
		ULONGLONG DiskSize;           // size of the drive, in bytes
		bResult = Harddisk::GetDriveGeometry(dsk, &pdg);
		if (bResult)
		{
			printf("%s\n", drive.c_str());
			printf("\tCylinders = %I64d\n", pdg.Cylinders);
			printf("\tTracks/cylinder = %ld\n", (ULONG)pdg.TracksPerCylinder);
			printf("\tSectors/track = %ld\n", (ULONG)pdg.SectorsPerTrack);
			printf("\tBytes/sector = %ld\n", (ULONG)pdg.BytesPerSector);
			DiskSize = pdg.Cylinders.QuadPart * (ULONG)pdg.TracksPerCylinder *(ULONG)pdg.SectorsPerTrack * (ULONG)pdg.BytesPerSector;
			printf("\tDisk size = %I64d (Bytes) = %I64d (Gb)\n", DiskSize, DiskSize / (1024 * 1024 * 1024));
		}
		else
		{
			//printf("GetDriveGeometry failed. Error %ld.\n", GetLastError());
			break;
		}
	}
}

short Harddisk::WriteSect(const char *dsk, char *&_buff, unsigned int noOfBytes, unsigned int offset) {
	DWORD dwWrite;
	//HANDLE hDisk = CreateFile(convertCharArrayToLPCWSTR(dsk), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	HANDLE hDisk = CreateFile(convertCharArrayToLPCWSTR(dsk), // drive
		GENERIC_READ | GENERIC_WRITE,                // no access to the drive
		FILE_SHARE_READ | // share mode
		FILE_SHARE_WRITE,
		NULL,             // default security attributes
		OPEN_EXISTING,   // disposition
		0,                // file attributes
		NULL);            // do not copy file attributes

	if (hDisk == INVALID_HANDLE_VALUE)
	{
		int lastError = GetLastError();
		cout << "unable to write harddisk ";
		if (lastError) {
			cout << "ERROR_ACCESS_DENIED" << endl;
		}
		else {
			cout << lastError << endl;
		}
		CloseHandle(hDisk);
		return 1;
	}
	SetFilePointer(hDisk, offset, 0, FILE_BEGIN); // which sector to write
	WriteFile(hDisk, _buff, noOfBytes, &dwWrite, 0);  // write sector
	cout << "write : " << dwWrite << " bytes" << endl;
	CloseHandle(hDisk);
	return 0;
}