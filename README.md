Pay tribute to John S. Fine's Partcopy. It is a great tool for us to manipulate harddisk sector when i was learning assembly programming in last century. Since partcopy can't work in latest windows, so i rewrite it from zero. You can use it to do these:

1. Create bootable disk for your assembly program
2. Backup some bytes from harddisk or USB stick

# Download:

* https://drive.google.com/open?id=1ZKms6Wi_Xov60H40G4112EcY0jdDGcnE

# USAGE:

```
Allowed options:
  --in arg
  --out arg
  --offset arg
  --bytes arg           no of bytes to read/write
  --help                help message
  --v                   version
  --l                   list all drives
```

## Example 1, copy bootsector from the first harddisk to file a.bin:
   petercopy --in \\.\PhysicalDrive0 -out a.bin -bytes 512

## Example 2, copy bootsector from file a.bin to first harddisk, becareful you harddisk will corrupt:
   petercopy --in a.bin -out \\.\PhysicalDrive0 -bytes 512

## Example 3, list all physical drives
   petercopy -l

